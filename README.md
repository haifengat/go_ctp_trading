# CTP接口下单

## 介绍

HFpy策略平台下单模块

## 软件架构

从HFpy的reids中订阅实时order，根据收到的order调用CTP接口下单、撤单、追单及算法下单。

## 使用说明

### 环境变量

变量名|说明|格式|示例
-|-|-|-
strategyName|需要监控的策略名列表|name1, name2...|
redisAddr|redis配置|ip:port|172.19.129.98:16379
tradeFront|CTP交易前置|tcp://ip:port|180.168.146.187:10101
quoteFront|CTP行情前置|tcp://ip:port|tcp://180.168.146.187:10111
loginInfo|登录信息配置|broker/investor/pwd/appid/authcode|9999/008105/1/simnow_client_test/0000000000000000
normal|默认追单|firstOffset, cancelSeconds, reorderOffset, reorderTimes|2, 3, 2, 5

## 镜像

``` bash
# 接口有更新时处理
\cp /root/go/pkg/mod/gitee.com/haifengat/goctp\@v0.6.5-20211215/lnx/*.so lib/
# 构建 可执行
go build -o go_ctp_trading main/main.go
docker build -t haifengat/go_ctp_trading:`date +%Y%m%d` . && docker push haifengat/go_ctp_trading:`date +%Y%m%d`
```

## 20210421

* SHFE|INE 区分平今平昨

## 20211014

* 修复追单逻辑
