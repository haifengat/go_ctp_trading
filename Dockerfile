FROM busybox:glibc
COPY ./Shanghai /usr/share/zoneinfo/Asia/
RUN ln -fs /usr/share/zoneinfo/Asia/Shanghai /etc/localtime

ARG appName
WORKDIR /app
COPY ./go_ctp_trading ./lib ./
# 交易日历，每年更新一次
# RUN yum install -y wget; \
#  wget http://data.haifengat.com/calendar.csv;
COPY ./calendar.csv ./
ENV LD_LIBRARY_PATH /app

COPY lib64 /lib64
ENTRYPOINT ["./go_ctp_trading"]
