package src

// Order 委托信号
type Order struct {
	ID         int `json:"ID"` // stra.ID*1000+len(orders)+1
	Instrument string
	Direction  string // Buy | Sell
	Offset     string // Open | Close
	Price      float64
	Volume     int
}
