.DEFAULT_GOAL := help
# 以目录名作为镜像，应用文件名
CURDIRNAME=$(shell basename ${CURDIR})
CURDATE=$(shell date '+%Y%m%d')
REGISTRY=haifengat
#===================================== git ================================#
gitpush: # git 代码提交并推送
	@if [ ! $M ]; then \
		echo "add param: M=<your comment for this commit>"; \
		exit 1; \
	fi
	git commit -a -m "${M}"
	git push origin
tag: # 添加标签（当前日期）
	# 删除当前日期的 tag
	- git tag -d v${CURDATE}
	- git push origin :refs/tags/v${CURDATE}
	git tag -a v${CURDATE} -m "$(shell git log -1 --pretty=%B)" # 最后提交注释作为tag注释
	git push origin --tags
#================================= 镜像处理 =============================#
build: # 编译 打包镜像
	\cp /root/go/pkg/mod/gitee.com/haifengat/goctp\@v0.6.5-20211215/lnx/*.so lib/
	go build -o ./${CURDIRNAME}
	@# 替换容器中的 appName 以解决 entrypoint 启动程序不能用变量的问题
	sed -i 's#$${appName}#${CURDIRNAME}#g' Dockerfile
	docker build . -t ${REGISTRY}/${CURDIRNAME}:${CURDATE} --build-arg appName=${CURDIRNAME}
docker: build # 镜像推送
	docker push ${REGISTRY}/${CURDIRNAME}:${CURDATE}
publish: docker # 镜像发送 latest
	docker tag ${REGISTRY}/${CURDIRNAME}:${CURDATE} ${REGISTRY}/${CURDIRNAME}:latest
	docker push ${REGISTRY}/${CURDIRNAME}:latest

.PHONY: gitpush tag build docker publish
.PHONY: help
help:
	@echo 'git提交并推送:      make gitpush M="提交说明"'
	@echo '创建tag(当前日期):  make tag'
	@echo 'build: 构建镜像'
	@echo 'publish: 镜像推送'
	@echo 'make -n 检查语法'
